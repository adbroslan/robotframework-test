*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported SeleniumLibrary.
Library           SeleniumLibrary
Library           AngularJSLibrary 

*** Variables ***
${SERVER}         doctor.biomarking.com/login
${BROWSER}        Chrome
${DELAY}          0
${VALID USER}     demo
${VALID PASSWORD}    mode
${LOGIN URL}      https://${SERVER}/
${ERROR URL}      https://${SERVER}/
*** Keywords ***
Open Browser To Login Page
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Maximize Browser Window
    Wait for Angular
    Login Page Should Be Open

Login Page Should Be Open
    Title Should Be    Doctor Portal | Biomark

Go To Login Page
    Go To    ${LOGIN URL}
    Login Page Should Be Open

Input Email
    [Arguments]    ${email}
    Input Text   model=lc.user.username    ${email}

Input Password
    [Arguments]    ${password}
    Input Text    model=lc.user.password    ${password}

Submit Credentials
    # Click Button    login
    Click Element  //button[contains(text(),'Login')]

# Welcome Page Should Be Open
#     Location Should Be    ${WELCOME URL}
#     Title Should Be    Welcome Page
